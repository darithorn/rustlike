extern crate sdl2;
mod vec2;
#[macro_use]
mod entity;
mod tilemap;
mod entities;
// mod player;
mod entitymanager;
mod spriterenderer;
mod sprite;
mod terminal;
mod fovmap;

use vec2::Vec2;
use terminal::*;
use entities::player::Player;
use entities::orc::Orc;
// use player::Player;
use entitymanager::EntityManager;
use spriterenderer::SpriteRenderer;
use spriterenderer::Section;
use tilemap::Tile;
use tilemap::Tilemap;
use sprite::Sprite;
use entity::*;

use std::clone::Clone;
use std::collections::HashMap;
use std::path::Path;

use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::surface::Surface;
use sdl2::keyboard::Keycode;

const WIN_WIDTH: u32 = 640;
const WIN_HEIGHT: u32 = 480;

#[derive(Clone)]
enum TileType {
    Floor,
    Wall,
    Door
}

#[derive(Clone)]
struct MyTile {
    tile_type: TileType,
    sprite: Sprite,
    solid: bool,
    open: bool
}

impl MyTile {
    fn Wall() -> Self {
        MyTile { tile_type: TileType::Wall, sprite: Sprite::Wall, solid: true, open: false }
    }

    fn Floor() -> Self {
        MyTile { tile_type: TileType::Floor, sprite: Sprite::Floor, solid: false, open: false }
    }

    fn Door() -> Self {
        MyTile { tile_type: TileType::Door, sprite: Sprite::ClosedDoor, solid: true, open: false }
    }
}

impl Tile for MyTile {
    fn sprite(&self) -> Sprite { self.sprite.clone() }
    fn is_solid(&self) -> bool { self.solid }
    fn done_action<'a>(&mut self, response: EntityResponse) -> Output {
        match response {
            EntityResponse::DoActionOnTile(_, action) => {
                match action {
                    TileAction::Bump => {
                        match self.tile_type {
                            TileType::Door => {
                                self.open = !self.open;
                                self.solid = !self.open;
                                if self.open { self.sprite = Sprite::OpenDoor; }
                                else { self.sprite = Sprite::ClosedDoor; }
                                return Output::None;
                            },
                            _ => {
                                return Output::Default;
                            }
                        }
                    },
                    TileAction::WalkedOn => {
                        // for traps
                    },
                    // _ => {}
                }
            },
            _ => {}
        }
        Output::Default
    }
    fn clone(&self) -> Box<Tile> {
        Box::new(MyTile {
            tile_type: self.tile_type.clone(),
            sprite: self.sprite.clone(),
            solid: self.solid,
            open: self.open
        }) as Box<Tile>
    }
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("Rustlike", WIN_WIDTH, WIN_HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut renderer = window.renderer().build().unwrap();

    let surf = Surface::load_bmp(Path::new("res/terminal.bmp")).unwrap();
    let texture = renderer.create_texture_from_surface(surf).unwrap();
    let mut sprite_renderer = SpriteRenderer::new(texture, (8, 8), (2, 2));
    sprite_renderer.insert(Sprite::Player, Section { coords: (4, 0), color: Some(Color::RGB(0, 0, 255)) });
    sprite_renderer.insert(Sprite::Orc, Section { coords: (4, 15), color: Some(Color::RGB(255, 0, 0)) });
    sprite_renderer.insert(Sprite::Wall, Section { coords: (2, 3), color: Some(Color::RGB(80, 80, 80)) });
    sprite_renderer.insert(Sprite::Floor, Section { coords: (2, 11), color: Some(Color::RGB(25, 25, 25)) });
    sprite_renderer.insert(Sprite::OpenDoor, Section { coords: (0, 0), color: None });
    sprite_renderer.insert(Sprite::ClosedDoor, Section { coords: (12, 5), color: Some(Color::RGB(150, 75, 0 )) });

    let terminal_surf = Surface::load_bmp(Path::new("res/terminal.bmp")).unwrap();
    let terminal_tex = renderer.create_texture_from_surface(terminal_surf).unwrap();
    let mut terminal = Terminal::new(terminal_tex, Vec2::new(0, WIN_HEIGHT as i32 - 100), Vec2::new(100, 100), 8, 16, (8, 8), (2, 2));
    // let tilemap_surf = Surface::load_bmp(Path::new("res/16x16.bmp")).unwrap();
    // let tilemap_tex = renderer.create_texture_from_surface(tilemap_surf).unwrap();
    // let mut sprite_renderer = SpriteRenderer::new(renderer, tilemap_tex, (16, 16));
    // sprite_renderer.insert(Sprite::Player, Section { coords: (0, 22), color: None, scale: Some((2, 2)) });
    // sprite_renderer.insert(Sprite::Orc, Section { coords: (7, 18), color: None, scale: Some((2, 2)) });
    // sprite_renderer.insert(Sprite::Wall, Section { coords: (14, 0), color: None, scale: Some((2, 2)) });
    // sprite_renderer.insert(Sprite::Floor, Section { coords: (12, 3), color: None, scale: Some((2, 2)) });
    // sprite_renderer.insert(Sprite::OpenDoor, Section { coords: (4, 2), color: None, scale: Some((2, 2)) });
    // sprite_renderer.insert(Sprite::ClosedDoor, Section { coords: (3, 2), color: None, scale: Some((2, 2)) });

    let mut tile_hash = HashMap::new();
    tile_hash.insert(Color::RGB(0, 0, 0), Box::new(MyTile::Wall()) as Box<Tile>);
    tile_hash.insert(Color::RGB(255, 255, 255), Box::new(MyTile::Floor()) as Box<Tile>);
    tile_hash.insert(Color::RGB(0, 255, 0), Box::new(MyTile::Door()) as Box<Tile>);

    let mut tilemap = Tilemap::from_bmp("res/Untitled.bmp", &tile_hash);

    let mut entity_manager = EntityManager::new(10, tilemap);
    entity_manager.push(Player::new(0, Vec2::new(5, 5)));
    entity_manager.push(Orc::new(1, Vec2::new(8, 8)));

    let player = entity_manager.find_entity_by_id(0).unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(Keycode::Right), .. } => {
                    player.borrow_mut().translate(Vec2::new(1, 0));
                },
                Event::KeyDown { keycode: Some(Keycode::Left), .. } => {
                    player.borrow_mut().translate(Vec2::new(-1, 0));
                },
                Event::KeyDown { keycode: Some(Keycode::Up), .. } => {
                    player.borrow_mut().translate(Vec2::new(0, -1));
                },
                Event::KeyDown { keycode: Some(Keycode::Down), .. } => {
                    player.borrow_mut().translate(Vec2::new(0, 1));
                },
                _ => {}
            }
        }
        renderer.set_draw_color(Color::RGB(0, 0, 0));
        renderer.clear();

        // {
        //     let mut player = entity_manager.entities.iter_mut().find(
        //     |e| {
        //         if e.id == 0 { true }
        //         else { false }
        //     }).unwrap();    
        //     sprite_renderer.draw_tilemap(&tilemap, player.fov_map());
        // }
        entity_manager.update(&mut terminal);
        entity_manager.draw(&mut sprite_renderer, &mut renderer);
        terminal.draw(&mut renderer, 10);

        renderer.present();
    }
}