extern crate sdl2;
// mod tilemap;
// mod entity;
// mod sprite;

use tilemap::Tilemap;
use entity::Entity;
use sprite::Sprite;
use vec2::Vec2;
use fovmap::FOVMap;

use std::cell::RefCell;
use std::sync::Arc;
use std::collections::HashMap;

use sdl2::rect::Rect;
use sdl2::render::Renderer;
use sdl2::render::Texture;
use sdl2::pixels::Color;

pub struct Section {
    pub coords: (i32, i32),
    pub color: Option<Color>
}

pub struct SpriteRenderer {
    // pub renderer: Renderer<'a>,
    pub spritesheet: Texture,
    pub sprite_size: (i32, i32),
    pub scale: (i32, i32),
    pub sprites: HashMap<Sprite, Section>
}

impl<'a> SpriteRenderer {
    pub fn new(spritesheet: Texture, sprite_size: (i32, i32), scale: (i32, i32)) -> Self {
        SpriteRenderer {
            // renderer: renderer,
            spritesheet: spritesheet,
            sprite_size: sprite_size,
            scale: scale,
            sprites: HashMap::new()
        }
    }

    pub fn insert(&mut self, key: Sprite, value: Section) {
        self.sprites.insert(key, value);
    }

    pub fn draw_sprite(&mut self, renderer: &mut Renderer<'a>, sprite: Sprite, pos: Vec2) {
        let section = match self.sprites.get(&sprite) {
            Some(i) => { i },
            None => {
                // I want it to panic to make it easy to debug missing sprites
                // Either that or draw a bright purple square
                panic!("No entry found for sprite: {:?}", sprite);
            }
        };
        match section.color {
            Some(Color::RGB(r, g, b)) => {
                self.spritesheet.set_color_mod(r, g, b);
            },
            _ => {
                self.spritesheet.set_color_mod(255, 255, 255);
            }
        }
        renderer.copy(&self.spritesheet,
                        Some(Rect::new( self.sprite_size.0 * section.coords.0, 
                                        self.sprite_size.1 * section.coords.1, 
                                        self.sprite_size.0 as u32, 
                                        self.sprite_size.1 as u32)),
                        Some(Rect::new( pos.x * self.scale.0 * self.sprite_size.0, 
                                        pos.y * self.scale.1 * self.sprite_size.1, 
                                        self.sprite_size.0 as u32 * self.scale.0 as u32, 
                                        self.sprite_size.1 as u32 * self.scale.1 as u32)));
    }

    pub fn draw_entity<'b>(&mut self, renderer: &mut Renderer<'a>, entity: &'b Entity) {
        self.draw_sprite(renderer, entity.sprite(), entity.pos());
    }

    pub fn draw_tilemap(&mut self, renderer: &mut Renderer<'a>, tilemap: Arc<RefCell<Tilemap>>, fov_map: &FOVMap) {
        let t = tilemap.borrow();
        let (w, h) = t.dimensions;
        for x in 0..w {
            for y in 0..h {
                //if fov_map.get(x, y) {
                    let tile = t.get(x, y);
                    self.draw_sprite(renderer, tile.unwrap().sprite(), Vec2::new(x, y));
                //}
            }
        }
    }
}