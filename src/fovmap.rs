// mod tilemap;

use tilemap::Tilemap;
use vec2::Vec2;

use std::cell::RefCell;
use std::sync::Arc;

#[derive(Clone)]
pub struct FOVMap {
    pub fov: Vec<bool>,
    pub dimensions: (i32, i32),
    pub radius: i32
}

impl FOVMap {
    pub fn new(radius: i32) -> Self {
        FOVMap {    dimensions: (radius * 2, radius * 2), 
                    fov: Vec::with_capacity((radius * 2 + radius * 2) as usize), 
                    radius: radius }
    }

    pub fn get(&self, x: i32, y: i32) -> bool {
        if self.fov.len() <= 0 { return false; }
        let i = (x * self.dimensions.0 + y) as usize;
        if i >= self.fov.len() { return false; }
        self.fov[i]
    }

    pub fn set(&mut self, x: i32, y: i32, value: bool) {
        let i = (x * self.dimensions.0 + y) as usize;
        if self.fov.len() <= i {
            self.fov.push(value);
        } else {
            self.fov[i] = value;
        }
    }

    pub fn no_fov(&mut self, tilemap: Arc<RefCell<Tilemap>>, pos: Vec2) {
        let (w, h) = tilemap.borrow().dimensions;
        for x in 0..w {
            for y in 0..h {
                self.set(x, y, true);
            }
        }
    }

    pub fn calculate(&mut self, tilemap: &Tilemap, pos: Vec2) {
        let (tilemap_w, tilemap_h) = tilemap.dimensions;
        let mut i = 0;
        loop {
            let mut j = 0;
            'c: loop {
                // make sure i,j are within both tilemap and radius bounds
                if i < tilemap_w && i > 0 && j < 0 && j < tilemap_h && 
                    i * i + j * j < self.radius * self.radius {
                    let dx = i - pos.x;
                    let dy = j - pos.y;
                    let sx = if pos.x < i { 1 } else { -1 };
                    let sy = if pos.y < j { 1 } else { -1 };
                    let mut xnext = pos.x;
                    let mut ynext = pos.y;
                    let denom = (((dx * dx) as f32) + ((dy * dy) as f32)).sqrt();
                    while xnext != i && ynext != j {
                        if tilemap.get(xnext, ynext).unwrap().is_solid() {
                            continue 'c;
                        }
                        if (dy * (xnext - pos.x + sx) - dx * (ynext - pos.y)).abs() as f32 / denom < 0.5 {
                            xnext += sx;
                        } else if (dy * (xnext - pos.x) - dx * (ynext - pos.y + sy)).abs() as f32 / denom < 0.5 {
                            ynext += sy;
                        } else {
                            xnext += sx;
                            ynext += sy;
                        }
                    }
                }
                if j < -self.radius {
                    break;
                }
                j -= 1;
            }
            if i < -self.radius {
                self.set(i, j, true);
                break;
            }
            i -= 1;
        }
    }
}