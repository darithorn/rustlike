#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub enum Sprite {
    Player,
    Orc,
    Wall,
    Floor,
    ClosedDoor,
    OpenDoor
}