// mod spriterenderer;
// mod entity;
// mod tilemap;

use spriterenderer::SpriteRenderer;
use entity::*;
use tilemap::Tilemap;
use terminal::*;
use vec2::Vec2;

use std::ops::Drop;
use std::ops::Deref;
use std::cmp::Ordering;
use std::cell::RefCell;
use std::sync::Arc;
use std::fmt::*;

use sdl2::render::Renderer;

pub struct EntityManager {
    tilemap: Arc<RefCell<Tilemap>>,
    entities: Vec<ObjectType>,
}

impl EntityManager {
    pub fn new(inital_size: usize, tilemap: Tilemap) -> Self {
        EntityManager { tilemap: Arc::new(RefCell::new(tilemap)), entities: Vec::with_capacity(inital_size) }
    }

    pub fn no_sort(lhs: ObjectType, rhs: ObjectType) -> Ordering {
        Ordering::Equal
    }

    pub fn push<T: 'static + Entity>(&mut self, entity: T) {
        self.entities.push(Arc::new(RefCell::new(entity)));
    }

    // fn sort<F>(&mut self, sort_fn: F)
    //                     where F: FnMut(ObjectType, ObjectType) -> Ordering {
    //     self.entities.sort_by(sort_fn);
    // }

    pub fn find_entity_by_id(&self, id: u32) -> Option<ObjectType> {
        // Option<&RefCell<Object>>
        match self.entities.iter().find(|e| {
            if (**e).borrow().id() == id { true } 
            else { false }
        }) {
            Some(o) => {
                Some((*o).clone())
            },
            None => {
                None
            }
        }
    }

    pub fn find_entity_by_loc(&self, pos: Vec2) -> Option<ObjectType> {
        match self.entities.iter().find(|e| {
            let epos = (**e).borrow().pos();
            if epos.x == pos.x && epos.y == pos.y { true }
            else { false }
        }) {
            Some(o) => {
                Some((*o).clone())
            },
            None => { None }
        }
    }

    pub fn update(&mut self, terminal: &mut Terminal) {
        let mut remove_entities = Vec::new();
        let len = self.entities.len();
        for i in 0..len {
            let requests: Vec<EntityRequest> = self.entities[i].borrow().requests().clone();
            // I clear here because the `response` might add additional flags
            // and we don't want to clear those
            self.entities[i].borrow_mut().requests_mut().clear();   
            for req in requests {
                match req {
                    EntityRequest::Tilemap => {
                        self.entities[i].borrow_mut().response(EntityResponse::Tilemap(self.tilemap.clone()));
                    },
                    EntityRequest::MoveTo(pos) => {
                        let tile_is_solid = match self.tilemap.borrow().get(pos.x, pos.y) {
                            Some(t) => { t.is_solid() },
                            // if None means we're on edge of map so default to solid
                            None => { true }
                        };
                        let can_move = self.find_entity_by_loc(pos).is_none() && !tile_is_solid;
                        self.entities[i].borrow_mut().response(EntityResponse::MoveTo(can_move, pos));
                    },
                    EntityRequest::EntityByLoc(pos) => {
                        let found_entity = self.find_entity_by_loc(pos);
                        self.entities[i].borrow_mut().response(EntityResponse::EntityByLoc(pos, found_entity));
                    },
                    EntityRequest::EntityByID(id) => {
                        let found_entity = self.find_entity_by_id(id);
                        self.entities[i].borrow_mut().response(EntityResponse::EntityByID(found_entity));
                    },
                    EntityRequest::DoActionOnEntity(entity, action) => {
                        // let mut e = ;
                        let output = entity.borrow_mut().response(EntityResponse::DoActionOnEntity(self.entities[i].clone(), action.clone()));
                        match output {
                            Output::Override(s) => {
                                terminal.push(s);
                            },
                            Output::Default => {
                                match &action {
                                    &EntityAction::ModifyHP(hp) => {
                                        terminal.push(&format!("Damaged {}", entity.borrow().name()));
                                    },
                                    // _ => {}
                                }
                            },
                            _ => {}
                        }
                    },
                    EntityRequest::DoActionOnTile(pos, action) => {
                        let mut tilemap = self.tilemap.borrow_mut();
                        let output = tilemap.get_mut(pos.x, pos.y).unwrap().done_action(EntityResponse::DoActionOnTile(self.entities[i].clone(), action));
                        match output {
                            Output::Override(s) => {
                                terminal.push(s);
                            },
                            Output::Default => {
                                match &action {
                                    &TileAction::Bump => {
                                        terminal.push("Ow!");
                                    },
                                    _ => {}
                                }
                            },
                            _ => {}
                        }
                    },
                    _ => {}
                }
            }
            if self.entities[i].borrow().is_dead() {
                remove_entities.push(i);
            } 
            self.entities[i].borrow_mut().update();
        }
        for i in remove_entities {
            terminal.push(&format!("{} died.", self.entities[i].borrow().name()));
            self.entities.remove(i);
        }
    }

    pub fn draw<'b>(&self, sprite_renderer: &mut SpriteRenderer, renderer: &mut Renderer<'b>) {
        {
            let player = self.find_entity_by_id(0).unwrap();
            sprite_renderer.draw_tilemap(renderer, self.tilemap.clone(), &player.borrow().fov_map());
        }
        for entity in self.entities.iter() {
            sprite_renderer.draw_entity(renderer, entity.borrow().deref());
        }
    }
}