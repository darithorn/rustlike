extern crate sdl2;
// mod sprite;

use sprite::Sprite;
use entity::EntityResponse;
use terminal::Output;

use std::path::Path;
use std::collections::HashMap;

use sdl2::pixels::Color;
use sdl2::surface::Surface;

pub trait Tile {
    fn sprite(&self) -> Sprite;
    fn is_solid(&self) -> bool;
    fn done_action<'a>(&mut self, response: EntityResponse) -> Output;
    fn clone(&self) -> Box<Tile>; 
}

pub struct Tilemap {
    pub map: Vec<Box<Tile>>,
    pub dimensions: (i32, i32)
}

impl Tilemap {
    pub fn new(dimensions: (i32, i32)) -> Self {
        Tilemap { map: Vec::with_capacity((dimensions.0 * dimensions.1) as usize), dimensions: dimensions }
    }

    pub fn with_data(map: Vec<Box<Tile>>, dimensions: (i32, i32)) -> Self {
        if (dimensions.0 * dimensions.1) as usize != map.len() {
            println!("The specified dimensions do not equal the length of the provided Vec!");
        }
        Tilemap { map: map, dimensions: dimensions }
    }

    pub fn from_bmp(filename: &str, sprite_colors: &HashMap<Color, Box<Tile>>) -> Self {
        let surface = match Surface::load_bmp(Path::new(filename)) {
            Ok(s) => s,
            Err(m) => panic!(m)
        };
        let mut map = Vec::with_capacity((surface.width() * surface.height()) as usize);
        surface.with_lock(|data| {
            for y in 0..surface.height() {
                for x in 0..surface.width() {
                    let offset: usize = (y as usize) * (surface.pitch() as usize) + (x as usize) * 3;
                    let item = sprite_colors.get(&Color::RGB(data[offset], data[offset + 1], data[offset + 2])).unwrap();
                    map.push((*item).clone());
                }
            }
        });
        Tilemap::with_data(map, (surface.width() as i32, surface.height() as i32))
    }

    pub fn push(&mut self, t: Box<Tile>) {
        if self.map.len() < (self.dimensions.0 * self.dimensions.1) as usize {
            self.map.push(t);
        }
    }

    pub fn get(&self, x: i32, y: i32) -> Option<&Box<Tile>> {
        if x > self.dimensions.0 || x < 0 || y > self.dimensions.1 || y < 0 { None }
        else { Some(&self.map[(x * self.dimensions.0 + y) as usize]) }
    }

    pub fn get_mut(&mut self, x: i32, y: i32) -> Option<&mut Box<Tile>> {
        if x > self.dimensions.0 || x < 0 || y > self.dimensions.1 || y < 0 { None }
        else { Some(&mut self.map[(x * self.dimensions.0 + y) as usize]) }
    }
}