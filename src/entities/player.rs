use sprite::Sprite;
use entity::*;
use fovmap::FOVMap;
use vec2::Vec2;
use terminal::Output;

struct_entity!(Player {

});
impl Player {
    pub fn new(id: u32, pos: Vec2) -> Self {
        Player {    id: id, 
                    requests: Vec::new(), 
                    fov_map: FOVMap::new(50), 
                    pos: pos,
                    hp: 10,
                    max_hp: 10,
                    name: "Player".to_string()
                }
    }
}

impl Entity for Player {
    impl_entity!(Sprite::Player);
    fn response(&mut self, res: EntityResponse) -> Output {
        match res {
            EntityResponse::Tilemap(tilemap) => {
                let pos = self.pos;
                self.fov_map.no_fov(tilemap.clone(), pos);
            },
            EntityResponse::MoveTo(b, pos) => {
                if b {
                    self.set_pos(pos);
                    self.requests.push(EntityRequest::DoActionOnTile(pos, TileAction::WalkedOn));
                } else {
                    self.requests.push(EntityRequest::EntityByLoc(pos));
                }
            },
            EntityResponse::EntityByID(entity) => {
                match entity {
                    Some(e) => {
                        println!("found entity {}", e.borrow().id());
                    },
                    None => {}
                }
            },
            EntityResponse::EntityByLoc(pos, entity) => {
                match entity {
                    Some(e) => {
                        self.requests.push(EntityRequest::DoActionOnEntity(e, EntityAction::ModifyHP(-5)));
                    },
                    None => {
                        self.requests.push(EntityRequest::DoActionOnTile(pos, TileAction::Bump));
                    }
                }
            },
            EntityResponse::DoActionOnEntity(entity, action) => {
                match action {
                    EntityAction::ModifyHP(hp) => {
                        let old_hp = self.hp();
                        self.set_hp(old_hp + hp);
                    }
                }
            }
            _ => {}
        }
        Output::Default
    }
    fn update(&mut self) {

    }
}