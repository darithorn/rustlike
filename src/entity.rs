// mod tilemap;
// mod sprite;
// mod fovmap;

use tilemap::Tilemap;
use sprite::Sprite;
use fovmap::FOVMap;
use vec2::Vec2;
use terminal::Output;

use std::cell::RefCell;
use std::sync::Arc;

// actions done to other entities
#[derive(Clone, Copy)]
pub enum EntityAction {
    ModifyHP(i32)
}

// actions doen to tiles
#[derive(Clone, Copy)]
pub enum TileAction {
    WalkedOn,
    Bump
}

#[derive(Clone)]
pub enum EntityRequest {
    Tilemap,
    MoveTo(Vec2),
    EntityByName(String),
    EntityByID(u32),
    EntityByLoc(Vec2),
    EntitiesByRadius(u32),
    DoActionOnEntity(ObjectType, EntityAction),
    DoActionOnTile(Vec2, TileAction) // Vec2 refers to the coords of the tile
}

pub enum EntityResponse {
    Tilemap(Arc<RefCell<Tilemap>>),
    MoveTo(bool, Vec2), // returns whether the entity can move there or not
    EntityByName(Option<ObjectType>),
    EntityByID(Option<ObjectType>),
    EntityByLoc(Vec2, Option<ObjectType>),
    EntitiesByRadius(Option<Vec<ObjectType>>),
    DoActionOnEntity(ObjectType, EntityAction), // ObjectType is the entity that performed the action
    DoActionOnTile(ObjectType, TileAction)
}

pub type ObjectType = Arc<RefCell<Entity>>;
pub trait Entity {
    fn name(&self) -> &String;
    fn hp(&self) -> i32;
    fn set_hp(&mut self, hp: i32);
    fn max_hp(&self) -> i32;
    fn id(&self) -> u32;
    fn response(&mut self, res: EntityResponse) -> Output;
    fn requests(&self) -> &Vec<EntityRequest>;
    fn requests_mut(&mut self) -> &mut Vec<EntityRequest>;
    fn sprite(&self) -> Sprite;
    fn pos(&self) -> Vec2;
    fn set_pos(&mut self, pos: Vec2);
    fn fov_map(&self) -> &FOVMap;
    fn fov_map_mut(&mut self) -> &mut FOVMap;
    fn update(&mut self);

    fn is_dead(&self) -> bool { self.hp() <= 0 }
    fn translate(&mut self, change: Vec2) {
        let pos = self.pos();
        self.set_pos(pos + change);
    }
}

#[macro_export]
macro_rules! struct_entity {
    ($name:ident { $($field_name:ident : $type_name:ident),* }) => (
        pub struct $name {
            pub id: u32,
            pub requests: Vec<EntityRequest>,
            pub fov_map: FOVMap,
            pub pos: Vec2,
            pub hp: i32,
            pub max_hp: i32,
            pub name: String,
            $(pub $field_name : $type_name),*
        }
    )
}
#[macro_export]
macro_rules! impl_entity {
    ($sprite:expr) => {
        fn name(&self) -> &String { &self.name }
        fn hp(&self) -> i32 { self.hp }
        fn set_hp(&mut self, hp: i32) { self.hp = hp; }
        fn max_hp(&self) -> i32 { self.max_hp }
        fn id(&self) -> u32 { self.id }
        fn sprite(&self) -> Sprite { $sprite }
        fn pos(&self) -> Vec2 { self.pos }
        fn requests(&self) -> &Vec<EntityRequest> { &self.requests }
        fn requests_mut(&mut self) -> &mut Vec<EntityRequest> { &mut self.requests }
        fn set_pos(&mut self, pos: Vec2) { 
            self.pos = pos;
            self.requests.push(EntityRequest::Tilemap);
        }
        fn translate(&mut self, pos: Vec2) {
            let epos = self.pos();
            self.requests.push(EntityRequest::MoveTo(epos + pos));
        }
        fn fov_map(&self) -> &FOVMap { &self.fov_map }
        fn fov_map_mut(&mut self) -> &mut FOVMap { &mut self.fov_map }
    }
}