use vec2::Vec2;
use sdl2::rect::Rect;
use sdl2::render::*;

#[derive(Clone, Copy)]
pub enum Output<'a> {
    None,
    Default,
    Override(&'a str)
}

pub struct Terminal {
    pos: Vec2, // Position in pixels
    size: Vec2, // Size in pixels

    lines: Vec<String>,
    line_height: i32,
    columns: i32,
    char_size: (i32, i32), // columns, char width, char height
    scale: (i32, i32),
    font_texture: Texture
}

impl Terminal {
    pub fn new(font_texture: Texture, pos: Vec2, size: Vec2, line_height: i32, columns: i32, char_size: (i32, i32), scale: (i32, i32)) -> Self {
        Terminal {  pos: pos, 
                    size: size, 
                    lines: Vec::new(), 
                    line_height: line_height,
                    columns: columns,
                    char_size: char_size,
                    scale: scale,
                    font_texture: font_texture }
    }

    pub fn push<'a>(&mut self, line: &'a str) {
        self.lines.push(line.to_string());
    }

    pub fn draw(&mut self, renderer: &mut Renderer, lines: u32) {
        let mut height: i32 = 0;
        if self.lines.is_empty() { return; }
        let max = if lines as usize >= self.lines.len() { self.lines.len() } else { lines as usize };
        let mut i = self.lines.len() - max;
        loop {
            let mut char_pos_x = 0;
            for b in self.lines[i].bytes() {
                // 10 is '\n'
                if b == 10 {
                    height += self.line_height * self.scale.1;
                    char_pos_x = 0;
                    continue;
                }
                let ch_x: i32 = b as i32 / self.columns;
                let ch_y: i32 = b as i32 % self.columns; // x == w
                renderer.copy(  &self.font_texture,
                                Some(Rect::new( ch_x * self.char_size.0, 
                                                ch_y * self.char_size.1, 
                                                self.char_size.0 as u32, 
                                                self.char_size.1 as u32)),
                                Some(Rect::new( self.pos.x + char_pos_x, 
                                                self.pos.y + height, 
                                                self.scale.0 as u32 * self.char_size.0 as u32, 
                                                self.scale.1 as u32 * self.char_size.1 as u32)));
                char_pos_x += self.char_size.0 * self.scale.0;
            }
            height += self.line_height * self.scale.1;
            if i >= self.lines.len() - 1 {
                break;
            }
            i += 1;
        }
    }
}